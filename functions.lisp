(defun all-required (a b)
    "All values required"
    (format t "~a ~a ~1%"a b))

(defun optional-args (a b &optional c)
    "Optional calue c"
    (format t "~a ~a ~a ~1%" a b c))

(defun optional-default (a b &optional (c 10))
    "Optional value c with default value 10"
    (format t "~a ~a ~a ~1%" a b c))

(defun optional-check (a b &optional (c 3 c-supplied-p))
    "optional value with a default and check to see if user supplied own value"
    (format t "~a ~a ~a ~a ~1%" a b c c-supplied-p))

(defun many (&rest vars)
    "Function with variable number of arguments stored in list vars"
    (format t "~a ~a ~%" (nth 1 vars) vars))

(defun keywords (&key a b c)
    "Function using keyword parameters, All optional"
    ;Avoid mixing optional and keyword parameter, will error on you
    (format t "~a ~a ~a ~%" a b c)
    (list a b c))

(defun foo (n)
    "Returning from a function in th emiddle of a function"
    (dotimes (i 10)
        (dotimes (j 10)
            (when (> (* i j) n)
                (return-from foo (list i j))))))

(defun plot (fn min max step)
    "passing function as a function parameter"
    (loop for i from min to max by step do
        (loop repeat (funcall fn i) do (format t "*"))
        (format t "~%")))


(all-required 2 3)
(optional-args 1 2)
(optional-args 1 2 3)
(optional-default 1 2)
(optional-default 1 2 22)
(optional-check 1 2)
(many 1 2 3 4 5 66 7 88 9)
(keywords :b 10)
(write (keywords :a 2 :b 19))
(terpri)
(write (foo 5))
(terpri)
(plot #'exp 0 4 1/2)
(terpri)
;passing an annoymous function instead 
;of defining a fucntion to never use again
(plot #'(lambda (x)(* 2 x)) 0 19 1)