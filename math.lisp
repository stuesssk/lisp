(write (+ 2 2))


; ~% is newline char
(format t "~%SIN pi/2~%")
(write (sin (/ pi 2)))


(format t "~%Log_10 10~%")

(write (log 10 10))

(setq PI (acos -1))
(format t "~%PI = ~F~%" PI)
