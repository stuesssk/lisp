(defun notint(user-input)
	"Function for when input is not and Integer"
	(format t "~s is not an Integer~%" user-input)
	(exit))


(princ "Enter Max: ")
(setq max (read))
(if (integerp max)
	(format t "Integer~%")
	(notint max))

(princ "Enter Min: ")
(setq min (read))
(if (integerp min)
	(format t "Integer~%")
	(notint min))


(format t " *|")
(loop for x from min to max 
	do(format t "~vd|" (+ 1 (log (expt 10 2)10)) x))
	
(terpri)
(loop for x from min to max 
	do(format t "~vd|" (log (expt 10 2)10) x)
	(loop for y from min to max 
		do(format t "~vd|" (+ 1 (log (expt 10 2)10)) (* x y)))
		(terpri))
;;;		(loop for i from 1 to (* 11(+ 2(log (* 10 10) 10)))
;;;			do (format t "_"))
;;;		(terpri))
