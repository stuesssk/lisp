
; Using do to calculate Fibonacci number
(do ((n 0 (1+ n))
    (cur 0 next)
    (next 1 (+ next cur)))
    ;can also have it return cur as return value ((test case) cur)
    ((= 10 n) (format t "~d~%" cur)))

;extended loop
(loop for i below 10
      and a = 0 then b
      and b = 1 then (+ a b)
      ; if needing return value use return finally (return a))
      finally (format t "~d~%" a))

;do loop not needing any looping variable (counter)
;(do ()
;    ((> (get-universal-time) *some-future-date*))
;   (format t "Waiting... ~%")
;   (sleep 60))

;(loop
;    (when(> (get-universal-time) *some-future-date*)
;    (return))
;   (format t "Waiting... ~%")
;   (sleep 60))



