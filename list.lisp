(list 1 2 3)
; Building a plist AKA lisp equivelent of python dictonary
(list :a 1 :b 2 :c 3 :d 4)

; Asking for the value of key c
(format t "~D" (getf (list :a 1 :b 2 :c 3 :d 4) :c))

(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))
  
(make-cd "Roses" "Kathy Mattea" 7 t)

;define global variable db to store a list of cd's
(defvar *db* nil)

(defun add-record (cd) (push cd *db*))

(add-record (make-cd "Roses" "Kathy Mattea" 7 t))
(add-record (make-cd "Fly" "Dixie Chicks" 8 t))
(add-record (make-cd "Home" "Dixie Chicks" 9 t))

(defun dump-db ()
  (dolist (cd *db*)
    (format t "˜{˜a:˜10t˜a˜%˜}˜%" cd)))
    
