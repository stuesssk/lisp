;assigning values to variables


;Defining global variables
; Standard parctace to bookend global vars with **
(defvar *count* 0
    "Running total")
(defparameter *gap-tolerance* .001
    "Allowable deviation")

;Defining constants
; + used to denote the variable is a constant
(defconstant +my-constant 1337)

;(let (variable*)
;   ...body-form)
(let ((x 10) (y 13) z)
    (format t "~a ~a ~a~%" x y z))

; variable only defined within scope of let
(defun foo (x)
    (format t "Parameter: ~a~%" x)
    (let ((x 10))
        (format t "Outer: ~a~%" x)
        (let ((x 20))
            (format t "Inner: ~a~%" x))
        (format t "Outer: ~a~%" x))
    (format t "Parameter: ~a~%" x))
(foo 1)

; Referencing previous let varibales in defining another
(let* ((x 10)
       (y (+ x 10)))
       (format t "~a ~a~%" x y))
;or nest your let
(let ((x 10))
    (let((y (+ x 10)))
    (format t "~a ~a~%" x y)))

;
(defun increment-count () (incf *count*))
(increment-count)
(format t "Count: ~a~%" *count*)


;Temporarily redifine standard output to send it to
; another stream ie. file
;(let (( *standar-output* *some-other-output*))
;    (stuff))


(defvar *x* 10)
(defun foo () (format t "X: ~d~%" *x*))

;With let, even a global variable is still
; only redifined within the scope of let
; even if the global variable is also incrementd as well
(defun bar()
    (foo)
    (let (( *x* 20)) (foo))
    (foo))
; once outside of the scope of the let the variable
; reverts back to the binding of the global scope
(bar)

(format t "PI: ~f" PI)

;setting a variable to another value
; equivilent to var = value in c
(setf x 2)
;setting 2 variable at the same time
(setf x 3 y 12)
;setting two variable to the same value
(setf x (setf y (random 10)))
;
;Incerement-> both are equivilent
(setf x (+ x 1))
(incf x)

(setf x (- x 1))
(decf x)

;increment by more than 1 (dec is the same)
(setf x (+ x 10))
(incf x 10)