(defun primep (n)
  "Is N prime?"
  (and (> n 1) 
       (or (= n 2) (oddp n))
       (loop for i from 3 to (isqrt n) by 2
	  never (zerop (rem n i)))))

(defun modulo-power (base power modulus)
  (loop with square = 1
        for bit across (format nil "~b" power)
        do (setf square (* square square))
        when (char= bit #\1) do (setf square (* square base))
        do (setf square (mod square modulus))
        finally (return square)))
 
(defun mersenne-prime-p (power)
  (do* ((N (1- (expt 2 power)))
        (sqN (isqrt N))
        (k 1 (1+ k))
        (q (1+ (* 2 power k)) (1+ (* 2 power k)))
        (m (mod q 8) (mod q 8)))
      ((> q sqN) (values t))
    (when (and (or (= 1 m) (= 7 m))
               (primep q)
               (= 1 (modulo-power 2 power q)))
      (return (values nil q)))))

(loop for p in '(2 3 4 5 7 11 13 17 19 23 29 31 37 41 43 47 53 929)
        do (multiple-value-bind (primep factor) 
               (mersenne-prime-p p)
             (format t "~&M~w = 2**~:*~w-1 is ~:[composite with factor ~w~;prime~]."
                     p primep factor)))
